/* root.c implements node root, the highest level of the application
   this node gets as input a binary file containing records of a given structure,
   a pattern(either numeric or a string) to be examined whether it appears in any field of the bin file as a substring
   and a certain depth for a BINARY search tree to create, so the number of leaf-nodes/searchers are known apriori
   then it creates a named-pipe and passes it to a splitter-merger node,a level below,
   he implements the searching part by splitting recursively into two processes each of them getting half of the file
   (or what -s option implies: each leaf gets an uneven amount of records to examine, this part is better explained in the readme)
   until the search tree reaches the depth given,there leaf-nodes do the searching and send a SIGUSR2 signal to the root just before exiting
   results(lines of the file) climb up the tree in a parent-child communication way using named-pipes and finally reach this node(root)
   so root collects them,gets the records of the bin file in those lines and writes them in a file called file.tmp
   then it creates a process implementing system program sort, passing file.tmp to it  
   sort prints them in the tty based on their 1st field (registration number) classification
   at last,it alternates itself with process rm deleting all the FIFO files created in the whole application
   Pretty much a version of grep for binary files that does not return garbage, splitting the searching procedure
   to a bunch of processes working concurrently
   It also prints some statistics of the procedure(avg CPU time for each leaf, SIGUSR2 signals received from leafs etc.) 
   root's pid is passed all the way down to the leaf-nodes so that they can send the SIGUSR2 signal to him 
*/
#include "record.h"
#define MIN_DEPTH 1
#define MAX_DEPTH 5
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define MIN(a, b) ((a) < (b) ? (a) : (b))

// handle errors concerning the arguments given and their ordering
void usage_error(char* exe)
{
    fprintf(stderr,"\e[1;31mUsage\e[0m: %s -h Height -d Datafile -p Pattern (-s)\n", exe);
    exit(EXIT_FAILURE);
}

// global variable counting the SIGUSR2 signals received from leaf-nodes
static int signals_received = 0;

// define a SIGUSR2 signal handler-counter
void handler(int sig){
    // re-establish(it was first done below) disposition of the signal SIGINT 
    // because the behavior of signal() varies across UNIX versions and the signal handler might be reset 
    if(signal(SIGUSR2, handler) == SIG_ERR){
        perror("root");
        exit(EXIT_FAILURE);
    }
    signals_received++;
}

int main(int argc,char* argv[]){
    clock_t start_t = time(NULL), end_t, total_t;
    int i, file = 0, height = 0, pattern = 0, sflag = 0, lines, fd, status, filedes[2];
    int lf_ns, sm_ns, lfs_total, sms_total, size, share, err, h;
    double lf_time, max_lf, min_lf, sm_time, max_sm, min_sm, temp;
    char str_rootId[6], str_lfs_total[3], str_sflag[2], str_share[8], str_lines[8], str_adjustHeight[2];
    char myFile[12], result[] = "file.tmp";
    pid_t pid, rootId = getpid();
    FILE* fp;
    record r; 
    snprintf(str_rootId, 6, "%d", (int)rootId);

    //establish disposition of the signal SIGINT 
    if(signal(SIGUSR2, handler) == SIG_ERR){
        perror("root");
        exit(EXIT_FAILURE);
    }

    // recognize all the arguments given in the tty
    for(i = 1; i < argc; i += 2){
        if(strcmp(argv[i],"-h") == 0)
            height = i + 1;
        else if(strcmp(argv[i],"-d") == 0)
            file = i + 1;
        else if(strcmp(argv[i],"-p") == 0)
            pattern = i + 1;
        else if(strcmp(argv[i],"-s") == 0)             // alternative splitting option requested
            sflag = 1;
    }

    // make sure the tree's height given is valid
    if((h = atoi(argv[height])) > MAX_DEPTH || h < MIN_DEPTH){
        fprintf(stderr,"\e[1;31mError\e[0m:  depth '%d' out of range [%d,%d]\n", h, MIN_DEPTH, MAX_DEPTH);
        exit(EXIT_FAILURE);
    }

    // estimate total leaves to be created --> 2^(depth) and convert to string in order to be passed on to the splitter-merger
    lfs_total = pow(2, h);   
    snprintf(str_lfs_total, 3, "%d", lfs_total);                   
    
    // convert s_flag to string in order to add it to the parameter list of the splitter-merger root is about to call
    snprintf(str_sflag, 2, "%d", sflag);

    // if any necessary flags were not specified call usage_error
    if( pattern == 0 || file == 0 || height == 0)
        usage_error(argv[0]);

    // open the binary file to search in
    fp = fopen(argv[file],"rb");
    if(fp == NULL){
        perror(argv[file]);
        exit(EXIT_FAILURE);
    }
    printf("\e[1;33mroot(%d)\e[0m: searching for string '%s' in binary file called '%s'\n", rootId, argv[pattern], argv[file]);

    // calculate the binary's file size in lines,so you can pass this number to the splitter-merger[1] node
    fseek(fp, 0, SEEK_END);
    size = ftell(fp);
    lines = (int)size/sizeof(record);
    snprintf(str_lines, 8, "%d", lines);
    rewind(fp);

    // now that the number of records is known, some skew cases must be excluded
    if(sflag == 1){
        // if the file does not contain enough records for a so big of a binary search tree to be created
        if(lines < lfs_total*(lfs_total+1)/2){
            fprintf(stderr,"\e[1;35mWarning\e[0m: %d searchers are too many to skew search %d lines\n"\
                           "\t  Even if share = 1, the i-th searcher will search i lines\n"\
                           "\t  So all together %d lines will be examined, but the file contains just"\
                           " %d lines/records.\n\t  Looking for an appropriate height...\n", lfs_total, lines, lfs_total*(lfs_total+1)/2, lines);
            lfs_total/=2;
            // adjust the height to the maximum appropriate
            while(lines < lfs_total*(lfs_total+1)/2)
                lfs_total/=2;
            if(lfs_total < 2){
                printf("\t   No appropriate height found.Exiting now...\n");
                fclose(fp);
                exit(EXIT_FAILURE);
            }
            else{
                snprintf(str_adjustHeight, 2, "%d", (int)(log(lfs_total) / log(2)));
                printf("\t  Search was adjusted to height: %s\n", str_adjustHeight);
                strcpy(argv[height],str_adjustHeight);
                snprintf(str_lfs_total, 3, "%d", lfs_total);
            }
        } 
    }    

    // calculate the share(size of the file) of each leaf is going to examine
    // more details about these mathematical formulas are available in the readme
    if(sflag == 0)
        share = (int)lines/lfs_total;                                   // even split
    else
        share = (int) (2*lines) / (lfs_total*(lfs_total+1));            // skew split
    if(share == 0)
        share = 1;
    snprintf(str_share, 8, "%d", share);                                // convert to string to add as a parameter
    printf("\e[1;33mroot(%d)\e[0m: file has %d lines\n", rootId, lines);
    if(sflag == 0)
        printf("\t    Balanced Searching: Each leaf will examine %d records\n", share);
    else
        printf("\t    Skew Searching: Each leaf will examine leaf_id*%d records\n", share);


    // create a named-pipe to communicate with sm[1] node
    // this file is uniquely identified by containing root's pid in its 
    snprintf(myFile, 12, "fileOf%d",(int)rootId);
    mkfifo(myFile,0666);

    // call sm[1] node
    if((pid = fork()) == 0){
        fclose(fp);
        // <usage> :  ./split_merge, <bin_file>, first_leaf_id   , last_leaf_id, 
        if((err=execl("./split_merge", "split_merge", argv[file], "1"             , str_lfs_total,\
                      //pattern    , height     ,named-pipe, rootId    , share_size, -s(0 or 1), 
                      argv[pattern], argv[height], myFile   , str_rootId, str_share , str_sflag ,\
                      //very_last_leaf,number_of_records
                      str_lfs_total   , str_lines       ,NULL))==-1){
            perror("root calling splitter-merger");
            exit(EXIT_FAILURE);
        }
    }     

    // open the file so that he can write in it and wait until child terminates, then turn off the timer and
    // read from the named-pipe until meeting 0 in it and write each line in file called file.tmp    
    if((fd = open(myFile,O_RDONLY|O_NONBLOCK)) == -1){
        perror("root");
        exit(EXIT_FAILURE);
    }
    waitpid(pid, &status, WUNTRACED | WCONTINUED);
    if(status >> 8 == EXIT_FAILURE){
        fprintf(stderr, "root: Child process %d terminated wildly\n",(int)pid);
        exit(EXIT_FAILURE);
    }
    end_t = time(NULL);
    total_t = (double)(end_t - start_t)/CLOCKS_PER_SEC;

    // a file for all records to be saved in is created      
    FILE* fpa = fopen(result,"w");
    while(read(fd, &i, sizeof(int)) > 0){
        if(i == 0)                                 // 0 indicates the end of the results(lines), CPU times follow
            break;
        fseek(fp,(i-1)*sizeof(record),SEEK_SET);  // get the i-th record by reading sizeof(record) bytes ,sizeof(record)*(i-1) bytes from SEEK_SET
        fread(&r, sizeof(record), 1, fp);
        fprintf(fpa, "%ld %s %s %s %d %s %s %f\n", 
        r.reg_number, r.first_name, r.last_name, r.street, r.home, r.city, r.postcode, r.salary);
    }

    // calculate min,max and avg time for splitters-mergers and leafs respectively
    lf_time = 0.0;                  // total leaf time initialized 
    lf_ns = 0;
    max_lf = 0.0;                   // Turnaround Time is an upperbound for both maximum leaf and sm CPU times
    min_lf = (double)total_t;                
    sm_time = 0;
    sm_ns = 0;
    max_sm = 0.0;
    min_sm = (double)total_t;    
    sms_total = pow(2, h)-1;        // total splitters-mergers = 2^depth-1

    // remember that total leaf nodes: lfs_total = 2^depth
    while(lf_ns < lfs_total && read(fd, &temp, sizeof(double)) > 0){
        max_lf = MAX(max_lf,temp);
        min_lf = MIN(min_lf,temp);
        lf_time += temp;
        lf_ns++;
    }
    while(sm_ns < sms_total && read(fd, &temp, sizeof(double)) > 0){
        max_sm = MAX(max_sm, temp);
        min_sm = MIN(min_sm, temp);
        sm_time += temp;
        sm_ns++;
    }

    close(fd);
    fclose(fpa);
    fclose(fp);

    // create a child process to execute system program 'sort' and pass the result to 'grep'
    printf("\n\e[1;33mroot(%d)\e[0m: collected the following results\n-------------------------------------------\n", rootId);
    if((pid = fork()) == 0){
        pipe(filedes);
        if((pid = fork()) == 0){
            close(filedes[0]);
            dup2(filedes[1], 1); 
            execlp("sort", "sort", "-g","-k", "1", result, NULL);
        }
        else{
            close(filedes[1]);
            dup2(filedes[0], 0);
            waitpid(pid, NULL, WUNTRACED | WCONTINUED);
            execlp("grep", "grep", "--color=auto", argv[pattern], NULL);
        }
    }
    waitpid(pid, &status, WUNTRACED|WCONTINUED);
    
    //print statistics and number of SIGUSR2 signals received
    printf("\n\e[1;33mroot(%d)\e[0m: CPU time statistics:\n\n\tsearchers\n\t---------\n\t\tmaximum CPU time: %lf\n"\
            "\t\tminimum CPU time: %lf\n\t\taverage CPU time: %lf\n\tsplitters-mergers\n"\
            "\t-----------------\n\t\tmaximum CPU time: %lf\n\t\tminimum CPU time: %lf\n"\
            "\t\taverage CPU time: %lf\n\tTurnaround Time: %lf\n",\
            rootId, max_lf, min_lf, lf_time/lfs_total, max_sm, min_sm, sm_time/sms_total, (double)total_t);
    
    printf("\n\e[1;33mroot(%d)\e[0m: SIGUSR2 signals from leaf-nodes received: %d\n", rootId, signals_received);

    // remove all named-pipes created in this whole thing
    execlp("make", "make", "purge", NULL);
    exit(EXIT_FAILURE);
}