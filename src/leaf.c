/* leaf.c implements leaf_node, the lowest level of the application
   this node gets a binary file containing records of a known structure and a pattern 'p'  
   for each line L containing a substing of 'p', it writes L's number in the FIFO file that was
   passed as a parameter by its parent(a splitter-merger node)
   leaf also gets it's leaf_id to identify its self amongst other leaves/searchers
   the size of the part of the file to search in and also the way(even or skew searching)
   Now leaf can estimate the range of the binary file to examine records at
   The last of all leaves will examine all remaining records,so no record will be ignored
   Just before exiting, leaf sends a SIGUSR2 signal to the root whose process Id is leaf's last parameter
*/
#include "record.h" 

int main(int argc,char* argv[]){
    
    // declare variables
    clock_t start_t = clock(), end_t, total_t;
    char am[20], pay[20], home_number[20];
    int line, numOrName, start, end, leaf_id = atoi(argv[2]), share = atoi(argv[3]);
    pid_t myId = getpid();
    record r;
    FILE* fp;

    // checking out if all arguments were passed correctly
    if(argc != 10){
        fprintf(stderr,"\e[1;31mError\e[0m: 'leaf_node': <Usage> %s file_name, leaf_id, share,"\
        " pattern, fifo-file_name, rootId, -s_flag(binary), last_leaf, number_of_records\n",argv[0]);
        exit(EXIT_FAILURE);
    }

    // estimate range to search in
    if(strcmp(argv[7], "0") == 0){
        // if the search is even (each leaf gets the same number of records = share)
        // then leaves from 1 to leaf_id-1 have searched or will search(leaves are concurrently executed) 
        // until the (leaf_id-1)*share-th record,so this leaf examines <share> records starting from the next one
        start = (leaf_id-1)*share + 1;
        end = start + share -1;
    }
    else{
        // if the search is skew each leaf examines a different kind of range depending on its leaf_id...more info in the readme
        start = share*(leaf_id-1)*leaf_id/2 + 1;
        end = start + leaf_id*share - 1;
    }

    // check whether we are searching for a number(0) or a name(1) 
    numOrName = atoi(argv[4]) == 0 ? 1 : 0;         
    
    // open binary file to read
    if( (fp = fopen(argv[1],"rb")) == NULL){                                    
        perror(argv[1]);
		exit(EXIT_FAILURE);
    }

    // get to the starting point given 
    fseek(fp, (start-1)*sizeof(record), SEEK_SET);                                  
    
    // open the fifo file ,the name of which was passed as a parameter, where all matching line's numbers will be written
    int fd = open(argv[5], O_WRONLY); 
    if(fd == -1){                                        
        fprintf(stderr,"\e[1;31mError\e[0m: fifo-file %s is inaccessible: process %d(leaf) - abort\n",argv[5], (int)myId);
        exit(EXIT_FAILURE);
    }

    // read all records till EOF or the last_one_ordered(included)
    line = start;  
    if(strcmp(argv[2],argv[8])==0)
        // if this is the last searcher,search until the EOF
        end = atoi(argv[9]); 

    printf("\e[1;32mleaf_%d(%d)\e[0m: searching in range [%d,%d] of the file\n", leaf_id, (int)myId, start, end);                                               
    while( (line <= end) && (fread(&r, sizeof(record), 1, fp) != 0) ){ 
        if(numOrName == 0){                                                     // if pattern is arithmetic
            snprintf(am, 20, "%ld", r.reg_number);                                  //      convert registration number,salary and home_id 
            snprintf(pay, 20, "%.2f", r.salary);                                    //      to string -> am, pay, home_number
            snprintf(home_number, 20, "%d", r.home);                                //      check whether pattern(argv[4]) is a substring of them or postcode's
            if(strstr(am, argv[4]) || strstr(pay, argv[4]) || strstr(home_number, argv[4]) || strstr(r.postcode, argv[4])) 
                write(fd,&line,sizeof(int));                                                    
        }                                                                    // else check whether it is a sub-string of the first or last name
        else{                                                                                   
            if(strstr(r.first_name, argv[4]) || strstr(r.last_name, argv[4]) || strstr(r.street, argv[4]) || strstr(r.city, argv[4]))                                                                             
                write(fd,&line,sizeof(int));                                                           
        }                                                                           
        line++; 
    }

    // add zero to the end of the line list to indicate its end 
    line = 0; 
    write(fd, &line, sizeof(int));

    // turn off the timer, calculate time spent, close files and send a signal to the root to pronounce tha the job is done
    sleep(leaf_id);
    end_t = clock();
    total_t = (double)(end_t - start_t)/CLOCKS_PER_SEC;
    write(fd, &total_t, sizeof(double));
    close(fd);
    fclose(fp); 
    kill( (pid_t)atoi(argv[6]), SIGUSR2); 
    exit(EXIT_SUCCESS);
}