#ifndef record_H
#define record_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h> 
#include <sys/stat.h> 
#include <sys/types.h> 
#include <sys/wait.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <math.h>
#include <signal.h>
#define BUFFSIZE 20
#define P_CODE_SIZE 6
#define COLOR_CODE \e[1;31m]
#define COLOR_LEN 13
#define FIELDS 5
#define MAX_DIGITS 10

typedef struct record{
    long reg_number;
    char last_name[BUFFSIZE];
    char first_name[BUFFSIZE];
    char street[BUFFSIZE];
    int  home;
    char city[BUFFSIZE];
    char postcode[P_CODE_SIZE];
    float salary;
} record;

#endif