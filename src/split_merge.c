/* split_merge.c implements node splitter-merger, the middle level of the application
   this node creates two named pipes using character array 'file' of type char[2][14] called fileOf<pid>_1 and fileOf<pid>_2 respectively
   then it 
   1. creates two child processes (either leaves if depth = 1 ,otherwise spliters-mergers)
   2. passes to them
            a. the binary file to be examined
            b. their lead_id that uniquely identifies them between other leaves 
            c. a variable called share and its value depends on the type of the search
               in an even searching each leaf gets one share(number of records)
               in a skew searching each leaf gets share*leaf_id records to examine
            d. the corresponding FIFO file names 
               also the pattern and search_depth
   3. opens the two FIFO files so that if children get blocked,they get unblocked immediately
   4. waits for them to finish so that we do not miss any of the results collected
   5. collects all the results and passes them up to his parent via the named-pipe given in his parameters
*/ 
#include "record.h"
   
int main(int argc, char* argv[]){

    // declare variables;
    clock_t start_t = clock(), end_t, total_t;
    char file[2][14], new_depth[2], start[2][3], end[2][3];
    int fd[2], status, fdUp, line, depth = atoi(argv[5]), split, err, i, j, times[2], counter;
    double child_time;
    pid_t prId[2], myId = getpid(), terminated;

    // checking out if all arguments were passed correctly
    if(argc != 12){
        fprintf(stderr,"\e[1;31mError\e[0m: 'splitter-merger node': <Usage> %s file_name, first_descendant,"\
        " last_descendant, pattern, search_depth, fifo-file_name, rootId, share, -s_flag(binary),"\
         "very_last_leaf, number_of_records\n",argv[0]);
        exit(EXIT_FAILURE);
    }
    
    // calculate and convert to string: a)the splitting_point and b) the new_depth = old_depth-1
    split = (int) (atoi(argv[2]) + atoi(argv[3])-1) / 2;
    strcpy(start[0], argv[2]);
    snprintf(end[0], 3,"%d", split);
    snprintf(start[1], 3, "%d", split+1);
    strcpy(end[1], argv[3]);
    printf("\e[1;34msplitter_merger(%d)\e[0m: splitting at leaf %d\n", (int)myId, split);
    snprintf(new_depth, 2, "%d", depth - 1);                                    

    // create two named-pipes to communicate with the process's 2 children
    // these are uniquely identified by containing the processe's id in their names
    snprintf(file[0], 14, "fileOf%d_1", (int)myId); 
    snprintf(file[1], 14, "fileOf%d_2", (int)myId);                             
    mkfifo(file[0], 0666);
    mkfifo(file[1], 0666);

    // create two processes,one of them will search in the first half(or whatever -s option requests) of the binary file
    // and the next will search for records having the pattern given as a substring in the other half of the file
    for(i = 0;i < 2;i++){
        if((prId[i] = fork()) == 0){
            // if depth = 1 child is a leaf else child is a splitter-merger with new_depth = old_depth - 1
            if(depth == 1){ 
                //                    ./leaf <bin_file>, leaf_id,  share  , pattern, named-pipe, rootId , -s_flag, last_leaf, sizeof(file)        
                if((err=execl("./leaf","leaf", argv[1],   start[i], argv[8], argv[4], file[i]   , argv[7], argv[9], argv[10] , argv[11], NULL))==-1){
                    perror("split_merge -> exec_leaf_i");
                    exit(EXIT_FAILURE);
                }                
            }
            else if((err = execl("./split_merge", "split_merge", argv[1], start[i], end[i], argv[4],\
                                 new_depth, file[i], argv[7], argv[8], argv[9], argv[10], argv[11], NULL)) == -1){
                    perror("split_merge -> exec_split_merge_i");
                    exit(EXIT_FAILURE);
            }                
        }
        if(prId[i] == -1){
            perror("fork()");
            exit(EXIT_FAILURE);
        }
        
        // after creating a child,parent opens his end of the named-pipe so that the child won't wait for it
        // if it gets the CPU control first
        fd[i] = open(file[i], O_RDONLY|O_NONBLOCK);
        if(fd[i] == -1){
            fprintf(stderr,"\e[1;34msplitter_merger(%d)\e[0m: \e[1;31mError\e[0m fifo-file %d is inaccessible - abort\n", (int)myId, i);
            exit(EXIT_FAILURE);
        }
    }

    // after creating the child nodes,sm opens his end of the pipe and WAITS until children are done
    for(i=0;i<2;i++){
        terminated = wait(&status);
        if(status >> 8 == EXIT_FAILURE){
            fprintf(stderr, "\e[1;34msplitter_merger(%d)\e[0m: Child process %d terminated wildly\n",(int)myId, (int)terminated);
            exit(EXIT_FAILURE);
        }
    }

    // after its children get those results ready, sm passes them to its parent by
    // 1.opening the FIFO file whose name was passed from above as an argument
    fdUp = open(argv[6], O_WRONLY);
    if(fdUp == -1){
        fprintf(stderr,"\e[1;34msplitter_merger(%d)\e[0m: \e[1;31mError\e[0m fifo-file %s is inaccessible - abort\n", (int)myId, argv[6]);
        exit(EXIT_FAILURE);
    }

    // 2.reading each record r from the FIFO file it created and writing it to the one passed from above
    for(i = 0;i < 2;i++){
        while(read(fd[i], &line, sizeof(int)) > 0){
            if(line == 0)                               // 0 indicates the end of the result list
                break;
            write(fdUp, &line, sizeof(int));
        }
    }

    // calculate how many split-merger and leaf-node CPU times were passed from below 
    // this part is better explained in the readme
    times[0] = pow(2, depth-1);     // number of leaves that passed up CPU times
    times[1] = pow(2, depth-1)-1;   // number of splitter-merger type nodes 

    // add zero to the end of the line list to indicate its end,then append the CPU times children returned
    line = 0;
    write(fdUp,&line, sizeof(int));
    // read times[0] times first from the FIFO of the child 0 and then from the FIFO of child 1
    // read times[1] times first from the FIFO of the child 0 and then from the FIFO of child 1
    // each time write the results in the other FIFO file,the one to pass above
    // that way CPU times of leaves go first just and sm's CPU times follow
    for(j = 0; j < 2; j++){         // j = 0 -> readingleaves, j = 1 -> reading sms
        for(i = 0;i < 2;i++){       // i = 0 -> reading from child 0, i = 1 from child[1]
            counter = 0;
            while(counter < times[j] && read(fd[i], &child_time, sizeof(double)) > 0){
                write(fdUp, &child_time, sizeof(double));
                counter++;
            }
        }
    }
    end_t = clock();
    total_t = (double)(end_t - start_t)/CLOCKS_PER_SEC; 
    write(fdUp, &total_t, sizeof(double));
    close(fd[0]);
    close(fd[1]); 
    close(fdUp);
    exit(EXIT_SUCCESS);
}