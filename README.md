# bgrep

## Intro

This is a pattern-searching application for binary files,
distributing the search load among an exponentially
increasing number of processes,
using named-pipes and POSIX signals.

## Run it

To run bgrep change to main directory and type `make`
for all executable files to be generated.  
After that, one can search for a pattern PATTERN in a binary
file under path PATH, with a search tree of height HEIGHT as follows:

    bgrep/main $ ./root -d PATH -h HEIGHT -p PATTERN

To enable **skew** search, include the -s flag.
In order to test bgrep, one can use the binary files under
`bgrep/rsrc` as input, consisting of fixed size records.
Do not hesitate to use even the largest of files.

To learn more about how bgrep works under the hood,
take a look at `bgrep/documentation/documentation.pdf`
